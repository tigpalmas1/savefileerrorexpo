import { StatusBar } from 'expo-status-bar';
import React, {useEffect} from 'react';
import { SafeAreaProvider } from 'react-native-safe-area-context';
// @ts-ignore


import useCachedResources from './hooks/useCachedResources';
import useColorScheme from './hooks/useColorScheme';
import Navigation from './navigation';
import i18n from "ex-react-native-i18n";
import * as Localization from 'expo-localization';
import Reactotron from "reactotron-react-native"

// @ts-ignore
export const LocalizationContext = React.createContext();

const en = {
  foo: 'Foo',
  bar: 'Pub {{someValue}}',
};

const fr = {
  foo: 'Fou',
  bar: 'Bár {{someValue}}',
};

const pt = {
  foo: 'Comida',
  bar: 'Boteco {{someValue}}',
};

i18n.fallbacks = true;
i18n.translations = { fr, en, pt };


if(__DEV__) {
  import("./ReactotronConfig")
}

export default function App() {

  Reactotron.display({
    name: "KNOCK KNOCK",
    preview: "Who's there?",
    value: "Orange."
  })
  // @ts-ignore
  const [locale, setLocale] = React.useState(Localization.locale);

  const localizationContext = React.useMemo(

      () => ({
        t: (scope, options) => i18n.t(scope, { locale, ...options }),
        locale,
        setLocale,
      }),
      [locale]
  );

  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();


  if (!isLoadingComplete) {
    return null;
  } else {
    return (
        <LocalizationContext.Provider value={localizationContext}>
      <SafeAreaProvider>
        <Navigation colorScheme={colorScheme} />
        <StatusBar />
      </SafeAreaProvider>
        </LocalizationContext.Provider>
    );
  }
}
