import Reactotron from 'reactotron-react-native';
import { NativeModules } from 'react-native';

// grabs the ip address
const host = NativeModules.SourceCode.scriptURL.split('://')[1].split(':')[0];

console.log(host);

const reactotron = Reactotron.configure({ host: '192.168.100.2', port: 9090 }).connect()

export default reactotron;