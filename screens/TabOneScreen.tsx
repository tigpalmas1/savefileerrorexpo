import * as React from 'react';
import {Button, Platform, StyleSheet} from 'react-native';

import {  View } from '../components/Themed';

import {LocalizationContext} from "../App";

import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import * as Notifications from 'expo-notifications';
import * as FileSystem from 'expo-file-system';
import * as MediaLibrary from 'expo-media-library';




export default function TabOneScreen(props) {
    const {navigation} = props ||{}
  // @ts-ignore
  const { t, locale, setLocale } = React.useContext(LocalizationContext);





    const downloadFile = ()=>{
        FileSystem.downloadAsync(
            'http://www.africau.edu/images/default/sample.pdf',
            FileSystem.documentDirectory + 'small.pdf'
        )
            .then(({ uri }) => {
                console.log('Finished downloading to ', uri);
                saveFileAsync(uri)

            })
            .catch(error => {
                console.error(error);
            });
    }

    const saveFileAsync = async (file_uri) =>{
        try {
            console.log('salvar arquivo');
            // const { status, permissions } = await MediaLibrary.requestPermissionsAsync();
            const { status, permissions } = await Permissions.askAsync(Permissions.MEDIA_LIBRARY);
            if (status === "granted" && permissions.mediaLibrary.accessPrivileges !== 'limited') {
                const asset = await MediaLibrary.createAssetAsync(file_uri);
                const album = await MediaLibrary.getAlbumAsync('Download');
                if (album === null) {
                    await MediaLibrary.createAlbumAsync('Download', asset, false);
                } else {
                    await MediaLibrary.addAssetsToAlbumAsync([asset], 'Download', false);
                }
                return true;
            }
            return false;
        } catch (error) {
            console.log('ERR: saveFileAsync', error);
            throw error;
        }
    }

  return (
      <View
          style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'space-around',
          }}>



          <Button
              title="Salvar arquivos"
              onPress={downloadFile}
          />
      </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  separator: {
    marginVertical: 30,
    height: 1,
    width: '80%',
  },
});



